import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/cardOne',
      name: 'cardOne',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/server/CardOne.vue')
    },
    {
      path: '/cardTwo',
      name: 'cardTwo',
      component: () => import('../views/server/CardTwo.vue')
    },
    {
      path: '/cardThree',
      name: 'cardthree',
      component: () => import('../views/server/CardThree.vue')
    },
    {
      path: '/cardFour',
      name: 'cardFour',
      component: () => import('../views/server/CardFour.vue')
    },
    {
      path: '/cardFive',
      name: 'cardFive',
      component: () => import('../views/server/CardFive.vue')
    },
    {
      path: '/spinnerOne',
      name: 'spinnerOne',
      component: () => import('../views/Spinner/SpinnerOne.vue')
    },
    {
      path: '/spinnerTwo',
      name: 'spinnerTwo',
      component: () => import('../views/Spinner/SpinnerTwo.vue')
    },
    {
      path: '/spinnerThree',
      name: 'spinnerThree',
      component: () => import('../views/Spinner/SpinnerThree.vue')
    },
    {
      path: '/spinnerFour',
      name: 'spinnerFour',
      component: () => import('../views/Spinner/SpinnerFour.vue')
    },
    {
      path: '/spinnerFive',
      name: 'spinnerFive',
      component: () => import('../views/Spinner/SpinnerFive.vue')
    }

  ]
})

export default router
