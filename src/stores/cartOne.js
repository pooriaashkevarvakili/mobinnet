
import { defineStore } from 'pinia'
import axios from 'axios'
function updateLocalStorage(posts) {
  localStorage.setItem('posts', JSON.stringify(posts))
}
export const useCounterStoreCardOne = defineStore('post', {
  state: () => {
    return {
      posts: localStorage.getItem('posts') ? JSON.parse(localStorage.getItem('posts')) : []
    }
  },
  getters: {
    getUsers(state) {
      return state.posts
    }
  },
  actions: {
    async fetchusers() {

      let token = 'PMAK-61325d70088f41003cdecd8a-dcb8940943ee694609eb0faf438043248d'
      try {
        const data = await axios.get('https://0bff10b1-9e94-477d-a69f-0e4c63078a37.mock.pstmn.io', {
          headers: {
            'Content-type': 'Application/json',
            "Access-Control-Allow-origin": "*",
            'x-api-key': token
          }
        })
        this.posts = data.data
        updateLocalStorage(this.posts)
      }
      catch (error) {
        alert(error)
        console.log(error)
      }
      if (axios.get('https://0bff10b1-9e94-477d-a69f-0e4c63078a37.mock.pstmn.io'), {
        headers: {
          'Content-type': 'Application/json',
          "Access-Control-Allow-origin": "*",
          'x-api-key': token
        }
      })
        this.posts = data.data
      updateLocalStorage(this.posts)
    }
  }

})
